package com.sda.corvus;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        printTable();
        printTableWithStringFormat();


    }

    public static void printTable(){
        System.out.printf("%s %15s\n","Exam name","Exam grade");
        System.out.println("------------------------------------");
        System.out.printf("%-20s %-5s\n","Java","A");
        System.out.printf("%-20s %-5s\n","Java","B");
        System.out.printf("%-20s %-5s\n","PHP","C");
        System.out.println("------------------------------------");
    }
    private static void printTableWithStringFormat(){
        System.out.println(String.format("%-20s %s", "Exam Name", "Exam Grade"));
        System.out.println("-------------------------------------------");
        System.out.println(String.format("%-24s %s", "Java", "A"));
        System.out.println(String.format("%-24s %s", "Php", "B"));
        System.out.println(String.format("%-24s %s", "VB net", "A"));
        System.out.println("-------------------------------------------");
    }



}
